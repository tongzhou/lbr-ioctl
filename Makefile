obj-m += chardev.o
mymodule-objs := chardev.o
KERNEL_PATH = /usr/src/kernels/3.10.0-693.el7.x86_64/

all:
	make -C $(KERNEL_PATH) M=$(PWD) modules

clean:
	make -C $(KERNEL_PATH) M=$(PWD) clean
