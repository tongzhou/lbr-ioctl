
chardev.o:     file format elf64-x86-64


Disassembly of section .text:

0000000000000000 <device_write>:
   0:	e8 00 00 00 00       	callq  5 <device_write+0x5>
   5:	55                   	push   %rbp
   6:	48 89 d1             	mov    %rdx,%rcx
   9:	31 c0                	xor    %eax,%eax
   b:	48 89 e5             	mov    %rsp,%rbp
   e:	41 54                	push   %r12
  10:	49 89 f4             	mov    %rsi,%r12
  13:	53                   	push   %rbx
  14:	48 89 d3             	mov    %rdx,%rbx
  17:	48 89 f2             	mov    %rsi,%rdx
  1a:	48 89 fe             	mov    %rdi,%rsi
  1d:	48 c7 c7 00 00 00 00 	mov    $0x0,%rdi
  24:	e8 00 00 00 00       	callq  29 <device_write+0x29>
  29:	48 85 db             	test   %rbx,%rbx
  2c:	74 67                	je     95 <device_write+0x95>
  2e:	bf 01 00 00 00       	mov    $0x1,%edi
  33:	31 c0                	xor    %eax,%eax
  35:	48 b9 00 00 00 00 00 	movabs $0x0,%rcx
  3c:	00 00 00 
  3f:	eb 17                	jmp    58 <device_write+0x58>
  41:	0f 1f 80 00 00 00 00 	nopl   0x0(%rax)
  48:	4c 8d 47 01          	lea    0x1(%rdi),%r8
  4c:	49 83 f8 51          	cmp    $0x51,%r8
  50:	74 2e                	je     80 <device_write+0x80>
  52:	48 89 f8             	mov    %rdi,%rax
  55:	4c 89 c7             	mov    %r8,%rdi
  58:	4c 01 e0             	add    %r12,%rax
  5b:	e8 00 00 00 00       	callq  60 <device_write+0x60>
  60:	48 39 df             	cmp    %rbx,%rdi
  63:	88 14 39             	mov    %dl,(%rcx,%rdi,1)
  66:	48 63 c7             	movslq %edi,%rax
  69:	75 dd                	jne    48 <device_write+0x48>
  6b:	48 c7 05 00 00 00 00 	movq   $0x0,0x0(%rip)        # 76 <device_write+0x76>
  72:	00 00 00 00 
  76:	5b                   	pop    %rbx
  77:	41 5c                	pop    %r12
  79:	5d                   	pop    %rbp
  7a:	c3                   	retq   
  7b:	0f 1f 44 00 00       	nopl   0x0(%rax,%rax,1)
  80:	48 c7 05 00 00 00 00 	movq   $0x0,0x0(%rip)        # 8b <device_write+0x8b>
  87:	00 00 00 00 
  8b:	b8 50 00 00 00       	mov    $0x50,%eax
  90:	5b                   	pop    %rbx
  91:	41 5c                	pop    %r12
  93:	5d                   	pop    %rbp
  94:	c3                   	retq   
  95:	31 c0                	xor    %eax,%eax
  97:	eb d2                	jmp    6b <device_write+0x6b>
  99:	0f 1f 80 00 00 00 00 	nopl   0x0(%rax)

00000000000000a0 <device_release>:
  a0:	e8 00 00 00 00       	callq  a5 <device_release+0x5>
  a5:	55                   	push   %rbp
  a6:	48 89 f2             	mov    %rsi,%rdx
  a9:	31 c0                	xor    %eax,%eax
  ab:	48 89 fe             	mov    %rdi,%rsi
  ae:	48 c7 c7 00 00 00 00 	mov    $0x0,%rdi
  b5:	48 89 e5             	mov    %rsp,%rbp
  b8:	e8 00 00 00 00       	callq  bd <device_release+0x1d>
  bd:	48 c7 c7 00 00 00 00 	mov    $0x0,%rdi
  c4:	83 2d 00 00 00 00 01 	subl   $0x1,0x0(%rip)        # cb <device_release+0x2b>
  cb:	e8 00 00 00 00       	callq  d0 <device_release+0x30>
  d0:	31 c0                	xor    %eax,%eax
  d2:	5d                   	pop    %rbp
  d3:	c3                   	retq   
  d4:	66 66 66 2e 0f 1f 84 	data32 data32 nopw %cs:0x0(%rax,%rax,1)
  db:	00 00 00 00 00 

00000000000000e0 <device_open>:
  e0:	e8 00 00 00 00       	callq  e5 <device_open+0x5>
  e5:	55                   	push   %rbp
  e6:	31 c0                	xor    %eax,%eax
  e8:	48 c7 c7 00 00 00 00 	mov    $0x0,%rdi
  ef:	48 89 e5             	mov    %rsp,%rbp
  f2:	e8 00 00 00 00       	callq  f7 <device_open+0x17>
  f7:	8b 15 00 00 00 00    	mov    0x0(%rip),%edx        # fd <device_open+0x1d>
  fd:	b8 f0 ff ff ff       	mov    $0xfffffff0,%eax
 102:	85 d2                	test   %edx,%edx
 104:	75 23                	jne    129 <device_open+0x49>
 106:	48 c7 c7 00 00 00 00 	mov    $0x0,%rdi
 10d:	c7 05 00 00 00 00 01 	movl   $0x1,0x0(%rip)        # 117 <device_open+0x37>
 114:	00 00 00 
 117:	48 c7 05 00 00 00 00 	movq   $0x0,0x0(%rip)        # 122 <device_open+0x42>
 11e:	00 00 00 00 
 122:	e8 00 00 00 00       	callq  127 <device_open+0x47>
 127:	31 c0                	xor    %eax,%eax
 129:	5d                   	pop    %rbp
 12a:	c3                   	retq   
 12b:	0f 1f 44 00 00       	nopl   0x0(%rax,%rax,1)

0000000000000130 <device_read>:
 130:	e8 00 00 00 00       	callq  135 <device_read+0x5>
 135:	55                   	push   %rbp
 136:	48 89 d1             	mov    %rdx,%rcx
 139:	31 c0                	xor    %eax,%eax
 13b:	48 89 e5             	mov    %rsp,%rbp
 13e:	41 55                	push   %r13
 140:	49 89 d5             	mov    %rdx,%r13
 143:	48 89 f2             	mov    %rsi,%rdx
 146:	41 54                	push   %r12
 148:	49 89 f4             	mov    %rsi,%r12
 14b:	48 89 fe             	mov    %rdi,%rsi
 14e:	53                   	push   %rbx
 14f:	48 89 fb             	mov    %rdi,%rbx
 152:	48 c7 c7 00 00 00 00 	mov    $0x0,%rdi
 159:	e8 00 00 00 00       	callq  15e <device_read+0x2e>
 15e:	4c 89 e2             	mov    %r12,%rdx
 161:	31 c0                	xor    %eax,%eax
 163:	4c 89 e9             	mov    %r13,%rcx
 166:	48 89 de             	mov    %rbx,%rsi
 169:	48 c7 c7 00 00 00 00 	mov    $0x0,%rdi
 170:	e8 00 00 00 00       	callq  175 <device_read+0x45>
 175:	48 8b 15 00 00 00 00 	mov    0x0(%rip),%rdx        # 17c <device_read+0x4c>
 17c:	0f b6 02             	movzbl (%rdx),%eax
 17f:	84 c0                	test   %al,%al
 181:	75 09                	jne    18c <device_read+0x5c>
 183:	31 c0                	xor    %eax,%eax
 185:	5b                   	pop    %rbx
 186:	41 5c                	pop    %r12
 188:	41 5d                	pop    %r13
 18a:	5d                   	pop    %rbp
 18b:	c3                   	retq   
 18c:	4d 85 ed             	test   %r13,%r13
 18f:	74 64                	je     1f5 <device_read+0xc5>
 191:	48 83 c2 01          	add    $0x1,%rdx
 195:	4c 89 ef             	mov    %r13,%rdi
 198:	eb 0f                	jmp    1a9 <device_read+0x79>
 19a:	48 83 c2 01          	add    $0x1,%rdx
 19e:	0f b6 42 ff          	movzbl -0x1(%rdx),%eax
 1a2:	84 c0                	test   %al,%al
 1a4:	74 43                	je     1e9 <device_read+0xb9>
 1a6:	49 89 f4             	mov    %rsi,%r12
 1a9:	49 89 d0             	mov    %rdx,%r8
 1ac:	49 8d 74 24 01       	lea    0x1(%r12),%rsi
 1b1:	4c 89 e1             	mov    %r12,%rcx
 1b4:	e8 00 00 00 00       	callq  1b9 <device_read+0x89>
 1b9:	48 83 ef 01          	sub    $0x1,%rdi
 1bd:	44 89 eb             	mov    %r13d,%ebx
 1c0:	29 fb                	sub    %edi,%ebx
 1c2:	48 85 ff             	test   %rdi,%rdi
 1c5:	75 d3                	jne    19a <device_read+0x6a>
 1c7:	48 89 15 00 00 00 00 	mov    %rdx,0x0(%rip)        # 1ce <device_read+0x9e>
 1ce:	45 31 ed             	xor    %r13d,%r13d
 1d1:	4c 89 ea             	mov    %r13,%rdx
 1d4:	89 de                	mov    %ebx,%esi
 1d6:	48 c7 c7 00 00 00 00 	mov    $0x0,%rdi
 1dd:	31 c0                	xor    %eax,%eax
 1df:	e8 00 00 00 00       	callq  1e4 <device_read+0xb4>
 1e4:	48 63 c3             	movslq %ebx,%rax
 1e7:	eb 9c                	jmp    185 <device_read+0x55>
 1e9:	4c 89 05 00 00 00 00 	mov    %r8,0x0(%rip)        # 1f0 <device_read+0xc0>
 1f0:	49 89 fd             	mov    %rdi,%r13
 1f3:	eb dc                	jmp    1d1 <device_read+0xa1>
 1f5:	31 db                	xor    %ebx,%ebx
 1f7:	eb d8                	jmp    1d1 <device_read+0xa1>
 1f9:	0f 1f 80 00 00 00 00 	nopl   0x0(%rax)

0000000000000200 <get_msr>:
 200:	e8 00 00 00 00       	callq  205 <get_msr+0x5>
 205:	55                   	push   %rbp
 206:	48 89 e5             	mov    %rsp,%rbp
 209:	48 83 ec 20          	sub    $0x20,%rsp
 20d:	8b 37                	mov    (%rdi),%esi
 20f:	65 48 8b 04 25 28 00 	mov    %gs:0x28,%rax
 216:	00 00 
 218:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
 21c:	31 c0                	xor    %eax,%eax
 21e:	48 c7 c7 00 00 00 00 	mov    $0x0,%rdi
 225:	89 75 e4             	mov    %esi,-0x1c(%rbp)
 228:	e8 00 00 00 00       	callq  22d <get_msr+0x2d>
 22d:	8b 4d e4             	mov    -0x1c(%rbp),%ecx
 230:	0f 32                	rdmsr  
 232:	89 55 e8             	mov    %edx,-0x18(%rbp)
 235:	89 45 f0             	mov    %eax,-0x10(%rbp)
 238:	48 8b 55 f0          	mov    -0x10(%rbp),%rdx
 23c:	48 8b 75 e8          	mov    -0x18(%rbp),%rsi
 240:	31 c0                	xor    %eax,%eax
 242:	48 c7 c7 00 00 00 00 	mov    $0x0,%rdi
 249:	e8 00 00 00 00       	callq  24e <get_msr+0x4e>
 24e:	48 8b 4d f8          	mov    -0x8(%rbp),%rcx
 252:	65 48 33 0c 25 28 00 	xor    %gs:0x28,%rcx
 259:	00 00 
 25b:	75 02                	jne    25f <get_msr+0x5f>
 25d:	c9                   	leaveq 
 25e:	c3                   	retq   
 25f:	90                   	nop
 260:	e8 00 00 00 00       	callq  265 <get_msr+0x65>
 265:	66 66 2e 0f 1f 84 00 	data32 nopw %cs:0x0(%rax,%rax,1)
 26c:	00 00 00 00 

0000000000000270 <device_ioctl>:
 270:	e8 00 00 00 00       	callq  275 <device_ioctl+0x5>
 275:	55                   	push   %rbp
 276:	31 c0                	xor    %eax,%eax
 278:	48 89 e5             	mov    %rsp,%rbp
 27b:	41 55                	push   %r13
 27d:	49 89 fd             	mov    %rdi,%r13
 280:	41 54                	push   %r12
 282:	49 89 d4             	mov    %rdx,%r12
 285:	89 f2                	mov    %esi,%edx
 287:	53                   	push   %rbx
 288:	89 f3                	mov    %esi,%ebx
 28a:	48 89 fe             	mov    %rdi,%rsi
 28d:	48 c7 c7 00 00 00 00 	mov    $0x0,%rdi
 294:	e8 00 00 00 00       	callq  299 <device_ioctl+0x29>
 299:	31 c0                	xor    %eax,%eax
 29b:	be 01 64 08 80       	mov    $0x80086401,%esi
 2a0:	48 c7 c7 00 00 00 00 	mov    $0x0,%rdi
 2a7:	e8 00 00 00 00       	callq  2ac <device_ioctl+0x3c>
 2ac:	81 fb 01 64 08 80    	cmp    $0x80086401,%ebx
 2b2:	0f 84 c0 00 00 00    	je     378 <device_ioctl+0x108>
 2b8:	76 46                	jbe    300 <device_ioctl+0x90>
 2ba:	81 fb 02 64 04 c0    	cmp    $0xc0046402,%ebx
 2c0:	74 2e                	je     2f0 <device_ioctl+0x80>
 2c2:	81 fb 03 64 10 c0    	cmp    $0xc0106403,%ebx
 2c8:	0f 85 8a 00 00 00    	jne    358 <device_ioctl+0xe8>
 2ce:	48 c7 c7 00 00 00 00 	mov    $0x0,%rdi
 2d5:	31 c0                	xor    %eax,%eax
 2d7:	e8 00 00 00 00       	callq  2dc <device_ioctl+0x6c>
 2dc:	4c 89 e7             	mov    %r12,%rdi
 2df:	e8 00 00 00 00       	callq  2e4 <device_ioctl+0x74>
 2e4:	5b                   	pop    %rbx
 2e5:	41 5c                	pop    %r12
 2e7:	41 5d                	pop    %r13
 2e9:	31 c0                	xor    %eax,%eax
 2eb:	5d                   	pop    %rbp
 2ec:	c3                   	retq   
 2ed:	0f 1f 00             	nopl   (%rax)
 2f0:	5b                   	pop    %rbx
 2f1:	41 0f be 84 24 00 00 	movsbl 0x0(%r12),%eax
 2f8:	00 00 
 2fa:	41 5c                	pop    %r12
 2fc:	41 5d                	pop    %r13
 2fe:	5d                   	pop    %rbp
 2ff:	c3                   	retq   
 300:	81 fb 00 64 08 80    	cmp    $0x80086400,%ebx
 306:	75 50                	jne    358 <device_ioctl+0xe8>
 308:	4d 89 e0             	mov    %r12,%r8
 30b:	4c 89 e0             	mov    %r12,%rax
 30e:	e8 00 00 00 00       	callq  313 <device_ioctl+0xa3>
 313:	84 d2                	test   %dl,%dl
 315:	0f 84 a6 00 00 00    	je     3c1 <device_ioctl+0x151>
 31b:	45 31 c9             	xor    %r9d,%r9d
 31e:	eb 06                	jmp    326 <device_ioctl+0xb6>
 320:	41 83 f9 4f          	cmp    $0x4f,%r9d
 324:	7f 14                	jg     33a <device_ioctl+0xca>
 326:	4c 89 c0             	mov    %r8,%rax
 329:	e8 00 00 00 00       	callq  32e <device_ioctl+0xbe>
 32e:	41 83 c1 01          	add    $0x1,%r9d
 332:	49 83 c0 01          	add    $0x1,%r8
 336:	84 d2                	test   %dl,%dl
 338:	75 e6                	jne    320 <device_ioctl+0xb0>
 33a:	49 63 d1             	movslq %r9d,%rdx
 33d:	4c 89 e6             	mov    %r12,%rsi
 340:	4c 89 ef             	mov    %r13,%rdi
 343:	31 c9                	xor    %ecx,%ecx
 345:	e8 b6 fc ff ff       	callq  0 <device_write>
 34a:	5b                   	pop    %rbx
 34b:	41 5c                	pop    %r12
 34d:	41 5d                	pop    %r13
 34f:	31 c0                	xor    %eax,%eax
 351:	5d                   	pop    %rbp
 352:	c3                   	retq   
 353:	0f 1f 44 00 00       	nopl   0x0(%rax,%rax,1)
 358:	89 de                	mov    %ebx,%esi
 35a:	48 c7 c7 00 00 00 00 	mov    $0x0,%rdi
 361:	31 c0                	xor    %eax,%eax
 363:	e8 00 00 00 00       	callq  368 <device_ioctl+0xf8>
 368:	5b                   	pop    %rbx
 369:	41 5c                	pop    %r12
 36b:	41 5d                	pop    %r13
 36d:	31 c0                	xor    %eax,%eax
 36f:	5d                   	pop    %rbp
 370:	c3                   	retq   
 371:	0f 1f 80 00 00 00 00 	nopl   0x0(%rax)
 378:	48 c7 c7 00 00 00 00 	mov    $0x0,%rdi
 37f:	31 c0                	xor    %eax,%eax
 381:	e8 00 00 00 00       	callq  386 <device_ioctl+0x116>
 386:	31 c9                	xor    %ecx,%ecx
 388:	ba 63 00 00 00       	mov    $0x63,%edx
 38d:	4c 89 e6             	mov    %r12,%rsi
 390:	4c 89 ef             	mov    %r13,%rdi
 393:	e8 98 fd ff ff       	callq  130 <device_read>
 398:	ba 7a 00 00 00       	mov    $0x7a,%edx
 39d:	48 63 c8             	movslq %eax,%rcx
 3a0:	4c 01 e1             	add    %r12,%rcx
 3a3:	89 d0                	mov    %edx,%eax
 3a5:	e8 00 00 00 00       	callq  3aa <device_ioctl+0x13a>
 3aa:	89 d0                	mov    %edx,%eax
 3ac:	e8 00 00 00 00       	callq  3b1 <device_ioctl+0x141>
 3b1:	31 c0                	xor    %eax,%eax
 3b3:	e8 00 00 00 00       	callq  3b8 <device_ioctl+0x148>
 3b8:	5b                   	pop    %rbx
 3b9:	41 5c                	pop    %r12
 3bb:	41 5d                	pop    %r13
 3bd:	31 c0                	xor    %eax,%eax
 3bf:	5d                   	pop    %rbp
 3c0:	c3                   	retq   
 3c1:	31 d2                	xor    %edx,%edx
 3c3:	e9 75 ff ff ff       	jmpq   33d <device_ioctl+0xcd>
 3c8:	0f 1f 84 00 00 00 00 	nopl   0x0(%rax,%rax,1)
 3cf:	00 

00000000000003d0 <test_msr>:
 3d0:	e8 00 00 00 00       	callq  3d5 <test_msr+0x5>
 3d5:	55                   	push   %rbp
 3d6:	48 c7 c7 00 00 00 00 	mov    $0x0,%rdi
 3dd:	48 89 e5             	mov    %rsp,%rbp
 3e0:	48 83 ec 18          	sub    $0x18,%rsp
 3e4:	65 48 8b 04 25 28 00 	mov    %gs:0x28,%rax
 3eb:	00 00 
 3ed:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
 3f1:	31 c0                	xor    %eax,%eax
 3f3:	e8 00 00 00 00       	callq  3f8 <test_msr+0x28>
 3f8:	b9 80 06 00 00       	mov    $0x680,%ecx
 3fd:	0f 32                	rdmsr  
 3ff:	89 55 e8             	mov    %edx,-0x18(%rbp)
 402:	89 45 f0             	mov    %eax,-0x10(%rbp)
 405:	48 8b 55 f0          	mov    -0x10(%rbp),%rdx
 409:	48 8b 75 e8          	mov    -0x18(%rbp),%rsi
 40d:	31 c0                	xor    %eax,%eax
 40f:	48 c7 c7 00 00 00 00 	mov    $0x0,%rdi
 416:	e8 00 00 00 00       	callq  41b <test_msr+0x4b>
 41b:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 41f:	65 48 33 04 25 28 00 	xor    %gs:0x28,%rax
 426:	00 00 
 428:	75 02                	jne    42c <test_msr+0x5c>
 42a:	c9                   	leaveq 
 42b:	c3                   	retq   
 42c:	0f 1f 40 00          	nopl   0x0(%rax)
 430:	e8 00 00 00 00       	callq  435 <test_msr+0x65>
 435:	66 66 2e 0f 1f 84 00 	data32 nopw %cs:0x0(%rax,%rax,1)
 43c:	00 00 00 00 

0000000000000440 <init_module>:
 440:	e8 00 00 00 00       	callq  445 <init_module+0x5>
 445:	55                   	push   %rbp
 446:	31 f6                	xor    %esi,%esi
 448:	49 c7 c0 00 00 00 00 	mov    $0x0,%r8
 44f:	48 c7 c1 00 00 00 00 	mov    $0x0,%rcx
 456:	ba 00 01 00 00       	mov    $0x100,%edx
 45b:	bf 64 00 00 00       	mov    $0x64,%edi
 460:	48 89 e5             	mov    %rsp,%rbp
 463:	53                   	push   %rbx
 464:	e8 00 00 00 00       	callq  469 <init_module+0x29>
 469:	85 c0                	test   %eax,%eax
 46b:	89 c3                	mov    %eax,%ebx
 46d:	79 1c                	jns    48b <init_module+0x4b>
 46f:	89 c2                	mov    %eax,%edx
 471:	48 c7 c6 00 00 00 00 	mov    $0x0,%rsi
 478:	48 c7 c7 00 00 00 00 	mov    $0x0,%rdi
 47f:	31 c0                	xor    %eax,%eax
 481:	e8 00 00 00 00       	callq  486 <init_module+0x46>
 486:	89 d8                	mov    %ebx,%eax
 488:	5b                   	pop    %rbx
 489:	5d                   	pop    %rbp
 48a:	c3                   	retq   
 48b:	ba 64 00 00 00       	mov    $0x64,%edx
 490:	48 c7 c6 00 00 00 00 	mov    $0x0,%rsi
 497:	48 c7 c7 00 00 00 00 	mov    $0x0,%rdi
 49e:	31 c0                	xor    %eax,%eax
 4a0:	31 db                	xor    %ebx,%ebx
 4a2:	e8 00 00 00 00       	callq  4a7 <init_module+0x67>
 4a7:	48 c7 c7 00 00 00 00 	mov    $0x0,%rdi
 4ae:	31 c0                	xor    %eax,%eax
 4b0:	e8 00 00 00 00       	callq  4b5 <init_module+0x75>
 4b5:	48 c7 c7 00 00 00 00 	mov    $0x0,%rdi
 4bc:	31 c0                	xor    %eax,%eax
 4be:	e8 00 00 00 00       	callq  4c3 <init_module+0x83>
 4c3:	48 c7 c7 00 00 00 00 	mov    $0x0,%rdi
 4ca:	31 c0                	xor    %eax,%eax
 4cc:	e8 00 00 00 00       	callq  4d1 <init_module+0x91>
 4d1:	ba 64 00 00 00       	mov    $0x64,%edx
 4d6:	48 c7 c6 00 00 00 00 	mov    $0x0,%rsi
 4dd:	48 c7 c7 00 00 00 00 	mov    $0x0,%rdi
 4e4:	31 c0                	xor    %eax,%eax
 4e6:	e8 00 00 00 00       	callq  4eb <init_module+0xab>
 4eb:	48 c7 c7 00 00 00 00 	mov    $0x0,%rdi
 4f2:	31 c0                	xor    %eax,%eax
 4f4:	e8 00 00 00 00       	callq  4f9 <init_module+0xb9>
 4f9:	48 c7 c7 00 00 00 00 	mov    $0x0,%rdi
 500:	31 c0                	xor    %eax,%eax
 502:	e8 00 00 00 00       	callq  507 <init_module+0xc7>
 507:	48 c7 c7 00 00 00 00 	mov    $0x0,%rdi
 50e:	31 c0                	xor    %eax,%eax
 510:	e8 00 00 00 00       	callq  515 <init_module+0xd5>
 515:	e9 6c ff ff ff       	jmpq   486 <init_module+0x46>
 51a:	66 0f 1f 44 00 00    	nopw   0x0(%rax,%rax,1)

0000000000000520 <cleanup_module>:
 520:	e8 00 00 00 00       	callq  525 <cleanup_module+0x5>
 525:	55                   	push   %rbp
 526:	48 c7 c1 00 00 00 00 	mov    $0x0,%rcx
 52d:	ba 00 01 00 00       	mov    $0x100,%edx
 532:	31 f6                	xor    %esi,%esi
 534:	bf 64 00 00 00       	mov    $0x64,%edi
 539:	48 89 e5             	mov    %rsp,%rbp
 53c:	e8 00 00 00 00       	callq  541 <cleanup_module+0x21>
 541:	5d                   	pop    %rbp
 542:	c3                   	retq   
